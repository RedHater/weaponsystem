import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeaponratingComponent } from './weaponrating/weaponrating.component';
import { WeaponlistComponent } from './weaponlist/weaponlist.component';
import { WeapondetailsComponent } from './weapondetails/weapondetails.component';
import { WeaponentityComponent } from './weaponentity/weaponentity.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { WeaponService } from './services/weaponservice';
import { WeaponratingeditComponent } from './weaponratingedit/weaponratingedit.component';
import { MaterialModule } from './app-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgChartsAngularModule } from 'ag-charts-angular';

@NgModule({
  declarations: [
    AppComponent,
    WeaponratingComponent,
    WeaponlistComponent,
    WeapondetailsComponent,
    WeaponentityComponent,
    WeaponratingeditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    BrowserAnimationsModule,
    AgChartsAngularModule
  ],
  providers: [WeaponService],
  bootstrap: [AppComponent]
})
export class AppModule { }
