import { Component, OnInit } from '@angular/core';
import { RatingService } from '../services/ratingService';
import { WeaponRating } from '../entities/weaponrating';

@Component({
  selector: 'app-weaponrating',
  templateUrl: './weaponrating.component.html',
  styleUrls: ['./weaponrating.component.css']
})
export class WeaponratingComponent implements OnInit {
  ratings:WeaponRating[];
  constructor(private ratingService:RatingService) { }

  ngOnInit(): void {
    this.loadData();
  }
  loadData(){
    this.ratingService.findAllInArray().subscribe(
      data=>{
        console.log(data);
        this.ratings = data;
        console.log(this.ratings)
      }
    );
  }

}
