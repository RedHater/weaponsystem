import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeaponratingComponent } from './weaponrating.component';

describe('WeaponratingComponent', () => {
  let component: WeaponratingComponent;
  let fixture: ComponentFixture<WeaponratingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeaponratingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeaponratingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
