import { Component, OnInit } from '@angular/core';
import { WeaponService } from '../services/weaponservice';
import { Weapon } from '../entities/weapon';


@Component({
  selector: 'app-weaponlist',
  templateUrl: './weaponlist.component.html',
  styleUrls: ['./weaponlist.component.css']
})
export class WeaponlistComponent implements OnInit {
  weapons?: Weapon[];
  constructor(private weaponService:WeaponService) { }

  ngOnInit(): void {
  this.loadAll();
  }
  loadAll(): any {
    this.weaponService.findAll().subscribe(
      data=>{
        console.log(data);
        this.weapons = data;
        console.log(this.weapons);
      }
    );
  }

}
