import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, ObservableInput } from 'rxjs';
import { map } from 'rxjs/operators';
import { Weapon } from '../entities/weapon';


type EntityResponseType = HttpResponse<Weapon>;

@Injectable({ providedIn: 'root' })
export class WeaponService {
  public resourceUrl = 'http://localhost:48888/weapons';

  constructor(protected http: HttpClient) {}

  postDataInDB(weapon: Weapon): Observable<EntityResponseType>{
    console.log(weapon);
     return this.http.post<Weapon>(this.resourceUrl+"/save", weapon, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Weapon>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  findAll():Observable<Weapon[]>{
    return this.http.get<GetResponse>(this.resourceUrl).pipe(
      map(res=>res._embedded.weapons)
    );
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
interface GetResponse{
  _embedded:{
    weapons:Weapon[];
  }
}
