import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, ObservableInput } from 'rxjs';
import { map } from 'rxjs/operators';
import { WeaponRating } from '../entities/weaponrating';


type EntityResponseType = HttpResponse<WeaponRating>;

@Injectable({ providedIn: 'root' })
export class RatingService{
    public resourceUrl = 'http://localhost:48888/weaponRatings';

    constructor(protected http: HttpClient) {}
  
    postDataInDB(weaponRating: WeaponRating):Observable<EntityResponseType>{
      console.log(weaponRating);
       return this.http.post<WeaponRating>(this.resourceUrl+'/save', weaponRating, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
      return this.http.get<WeaponRating>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    findAll():Observable<WeaponRating[]>{
      return this.http.get<GetResponse>(this.resourceUrl).pipe(
        map(res=>res._embedded.weaponRating)
      );
    }
    findAllInArray():Observable<WeaponRating[]>{
      return this.http.get<WeaponRating[]>(this.resourceUrl+'/all');
    }

    getWeaponFromRating(id:number){
      return this.http.get(this.resourceUrl+'/'+id+'/weapon');
    }

    delete(id: number): Observable<HttpResponse<{}>> {
      return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    getAVGRate(weaponId:number): Observable<WeaponRating>{
      return this.http.get<WeaponRating>(this.resourceUrl+"/avgrate/"+weaponId);
    }

  }
  interface GetResponse{
    _embedded:{
        weaponRating:WeaponRating[];
    }
  }
