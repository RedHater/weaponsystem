import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { WeaponService } from '../services/weaponservice';
import { ActivatedRoute } from '@angular/router';
import { Weapon } from '../entities/weapon';
import { ImageService } from '../services/imageService';
import { RatingService } from '../services/ratingService';
import { WeaponRating } from '../entities/weaponrating';
@Component({
  selector: 'app-weapondetails',
  templateUrl: './weapondetails.component.html',
  styleUrls: ['./weapondetails.component.css']
})
export class WeapondetailsComponent implements OnInit {

  weapon:Weapon;
  rating: WeaponRating;
  options1:any;
  image:any;
  constructor(
     private httpClient: HttpClient,
     private weaponService:WeaponService,
     private route: ActivatedRoute,
     private imageService:ImageService,
     private ratingService:RatingService
     ) { }
  
  ngOnInit(): void {
    this.loadWeapon(this.route.snapshot.params['id']);
  }
  loadWeapon(id:number){
    this.weaponService.find(id).subscribe(
      data=>{
        this.weapon = data.body;
        this.loadWeaponRating(this.weapon.id);
        console.log(data.body);
        this.imageService.getImageFromWeapon(data.body.id).subscribe(
          data=>{
            this.imageService.getImageByName(data.name).subscribe(
              data=>{
                this.image = 'data:image/jpeg;base64,'+ data.picByte;
              }
            )
          }
        );
      }
    );
  }

  loadWeaponRating(weaponId){
    this.ratingService.getAVGRate(weaponId).subscribe(
      data=>{
      console.log(data);
      let data1 = [
        { name: 'Рейтинг', value: data.rating },
        { name: 'Дальность', value: data.range },
        { name: 'Защита', value: data.defense },
        { name: 'Прочность', value: data.durability },
        { name: 'Вариативность', value: data.fintVariety },
        { name: 'Темп', value: data.pace },
        { name: 'Легкость ремонта', value: data.repairAbility },
        { name: 'Безопасность', value: data.safety },
        { name: 'Скорость 1 удара', value: data.singleHitSpeed },
        { name: 'Требования к физ.подготовке', value: data.requirementsForStrengthAndEndurance },
        { name: 'Требования к опыту', value: data.requirementForExp },
        { name: 'Точность', value: data.accuracy },
      ];
      this.options1 = {
        data: data1,
        background:[
          {
            visible:false,
          },
        ],
        series: [
          {
            type: 'bar',
            xKey: 'name',
            yKeys: ['value'],
            fills: ['rgba(0, 117, 163, 0.9)'],
          strokes: ['rgba(0, 117, 163, 0.9)'],
          },
        ],
        axes: [
          {
            type: 'category',
            position: 'left',
          },
          {
            type: 'number',
            position: 'bottom',
          },
        ],
        legend: { enabled: false },
      };
    }
    );
  }

}
