import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeapondetailsComponent } from './weapondetails/weapondetails.component';
import { WeaponlistComponent } from './weaponlist/weaponlist.component';
import { WeaponratingComponent } from './weaponrating/weaponrating.component';
import { WeaponentityComponent } from './weaponentity/weaponentity.component';
import { WeaponratingeditComponent } from './weaponratingedit/weaponratingedit.component';


const routes: Routes = [
{ path: '', redirectTo: '/', pathMatch: 'full' },
{ path: 'weapon/:id', component: WeapondetailsComponent },
{ path: 'weaponsentity/:id', component: WeaponentityComponent },
{ path: 'weapons', component: WeaponlistComponent },
{ path: 'rating', component: WeaponratingComponent },
{ path: 'rating/:id', component: WeaponratingeditComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
