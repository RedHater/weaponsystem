import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RatingService } from '../services/ratingService';
import { WeaponRating } from '../entities/weaponrating';
import { WeaponService } from '../services/weaponservice';
import { Weapon } from '../entities/weapon';

@Component({
  selector: 'app-weaponratingedit',
  templateUrl: './weaponratingedit.component.html',
  styleUrls: ['./weaponratingedit.component.css']
})
export class WeaponratingeditComponent implements OnInit {
  weapons:Weapon[];
  rating:WeaponRating;
  new:boolean;
  wpStr:string;
  constructor( 
    private route: ActivatedRoute,
    private ratingService:RatingService,
    private weaponService:WeaponService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(){
    if(this.route.snapshot.params['id']=='new'){
      this.rating = new WeaponRating(5,5,5,5,5,5,5,5,5,5,5,5,5,null);
      this.new =true;
    }else{
      this.getRatingData();
    }
    this.weaponService.findAll().subscribe(
      data=>{
        this.weapons = data;
      }
    );
  }

  getRatingData(){
    this.ratingService.find(this.route.snapshot.params['id']).subscribe(
      data=>{
        this.rating = data.body;
        this.ratingService.getWeaponFromRating(this.rating.id).subscribe(
          data=>{
            this.rating.weapon = data;
            this.wpStr = this.rating.weapon.id+":"+this.rating.weapon.name;
          }
        );
      }
    );
  }

  saveRating(){
    console.log(this.weapons);
    this.rating.weapon=new Weapon();
    this.rating.weapon.id=+this.wpStr.substring(0,this.wpStr.indexOf(":"));
    if(this.new){
      this.ratingService.postDataInDB(this.rating).subscribe(s=>{
        this.router.navigateByUrl("rating");
      });
      console.log(this.rating.weapon.id);
    }else{
      this.ratingService.postDataInDB(this.rating).subscribe(s=>{
        this.router.navigateByUrl("rating");
      }
      );
    }
    
  }
}
