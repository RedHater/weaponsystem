import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeaponratingeditComponent } from './weaponratingedit.component';

describe('WeaponratingeditComponent', () => {
  let component: WeaponratingeditComponent;
  let fixture: ComponentFixture<WeaponratingeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeaponratingeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeaponratingeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
