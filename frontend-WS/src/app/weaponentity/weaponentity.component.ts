import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WeaponService } from '../services/weaponservice';
import { ActivatedRoute, Router, UrlHandlingStrategy } from '@angular/router';
import { Weapon,Hands } from '../entities/weapon';
import { Image } from '../entities/image';
import { ImageService } from '../services/imageService';
import { CDK_CONNECTED_OVERLAY_SCROLL_STRATEGY_PROVIDER } from '@angular/cdk/overlay/overlay-directives';

@Component({
  selector: 'app-weaponentity',
  templateUrl: './weaponentity.component.html',
  styleUrls: ['./weaponentity.component.css']
})
export class WeaponentityComponent implements OnInit {

  constructor(
    private weaponService:WeaponService,
    private route: ActivatedRoute,
    private imageService:ImageService,
    private router: Router) { }
  maxByte=1048576;
  weapon:Weapon;
  new:boolean;
  newImage:boolean;
  max:boolean;
  hands=Hands;
  selectedFile: File;
  retrievedImage:any;
  ngOnInit(): void {
    this.weapon = new Weapon();
    if(this.route.snapshot.params['id']=='new'){
      this.new = true;
    }
    else
    { 
      this.getWeaponData();
    }
  }

  public onFileChanged(event) {
    //Select File
    this.newImage=true;
    this.selectedFile = event.target.files[0];
    const file = (event.target as HTMLInputElement).files[0];
    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.retrievedImage = reader.result as string;
    }
    reader.readAsDataURL(file)
  }

  saveWeapon(){
    let result = new Weapon();
    result.id = this.weapon.id;
    result.name = this.weapon.name;
    result.description = this.weapon.description;
    result.avatar = this.weapon.avatar;
    console.log(this.weapon);
  if(this.retrievedImage!=null)
    {
       if(this.new || this.newImage){
         if(this.selectedFile.size<=this.maxByte){
          this.imageService.loadImage(this.selectedFile).subscribe(
            data=>{
              result.avatar=data.body;
              result.handsCount = this.weapon.handsCount.toUpperCase();
          this.weaponService.postDataInDB(result).subscribe(s=>{
            this.router.navigateByUrl(`weapons`)});
              }
          );
          }else{
            this.max=true;
          }
      }else{
        result.handsCount = this.weapon.handsCount.toUpperCase();
        this.weaponService.postDataInDB(result).subscribe(s=>{
          this.router.navigateByUrl(`weapons`)
        }); 
      }
    }
  }
  
  getWeaponData(){
    this.weaponService.find(this.route.snapshot.params["id"]).subscribe(
      data=>{
        this.weapon.id = data.body.id;
        this.weapon.name = data.body.name;
        this.weapon.description = data.body.description;
        this.weapon.handsCount = data.body.handsCount;
        this.weapon.handsCount=data.body.handsCount.substring(0,1).concat(data.body.handsCount.substring(1).toLowerCase());
        console.log(this.weapon);
        this.imageService.getImageFromWeapon(data.body.id).subscribe(
          data=>{
               this.weapon.avatar = data;
               this.imageService.getImageByName(data.name).subscribe(
                 data=>{
                  this.retrievedImage = 'data:image/jpeg;base64,'+ data.picByte;
                 }
               );
          }
        );
      }
    );
  }
}
