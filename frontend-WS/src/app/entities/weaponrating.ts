import { Weapon } from './weapon';

export class WeaponRating {
    constructor(
      public id?: number,
      public requirementsForStrengthAndEndurance?: number,
      public requirementForExp?: number,
      public range?: number,
      public durability?: number,
      public repairAbility?: number,
      public pace?: number,
      public singleHitSpeed?: number,
      public defense?: number,
      public accuracy?: number,
      public fintVariety?: number,
      public safety?: number,
      public rating?:number,
      public weapon?: Weapon
    ) {}
}