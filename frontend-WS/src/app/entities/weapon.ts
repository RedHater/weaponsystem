import { Image } from './image';

export const Hands: any = ["Две", "Вариативно", "Одна"];

export class Weapon{
    constructor(
      public id?: number,
      public name?: string,
      public avatar?: Image,
      public description?: string,
      public handsCount?: string
    ) {}
  }