package com.tengu.weaponsystem.service;

import com.tengu.weaponsystem.entity.WeaponRating;
import com.tengu.weaponsystem.projection.AVGRateProjection;

import java.util.Optional;

public interface WeaponRatingService {

    WeaponRating save(WeaponRating weaponEstimate);

    Iterable<WeaponRating> findAll();

    Optional<WeaponRating> findOne(Long id);

    void delete(Long id);

    AVGRateProjection getAVGList(Long weaponId);
}
