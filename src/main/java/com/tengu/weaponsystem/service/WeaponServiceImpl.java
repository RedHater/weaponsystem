package com.tengu.weaponsystem.service;

import com.tengu.weaponsystem.entity.Weapon;
import com.tengu.weaponsystem.entity.WeaponRating;
import com.tengu.weaponsystem.repository.WeaponRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class WeaponServiceImpl implements WeaponService {
    private final WeaponRepository weaponRepository;
    private final EntityManager entityManager;

    public WeaponServiceImpl(WeaponRepository weaponRepository, EntityManager entityManager) {
        this.weaponRepository = weaponRepository;
        this.entityManager = entityManager;
    }

    @Override
    public Weapon save(Weapon weapon) {
        return weaponRepository.save(weapon);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Weapon> findOne(Long id) {
        return weaponRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Weapon> findAll() { ;
        return weaponRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        weaponRepository.deleteById(id);
    }

}
