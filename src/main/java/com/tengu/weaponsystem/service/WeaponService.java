package com.tengu.weaponsystem.service;

import com.tengu.weaponsystem.entity.Weapon;

import java.util.List;
import java.util.Optional;

public interface WeaponService {

    Weapon save(Weapon weapon);

    Iterable<Weapon> findAll();

    Optional<Weapon> findOne(Long id);

    void delete(Long id);

}
