package com.tengu.weaponsystem.service;

import com.tengu.weaponsystem.entity.WeaponRating;
import com.tengu.weaponsystem.projection.AVGRateProjection;
import com.tengu.weaponsystem.repository.WeaponRatingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class WeaponRatingServiceImpl implements WeaponRatingService {
    private final WeaponRatingRepository weaponRatingRepository;

    public WeaponRatingServiceImpl(WeaponRatingRepository weaponRatingRepository) {
        this.weaponRatingRepository = weaponRatingRepository;
    }

    @Override
    public WeaponRating save(WeaponRating weaponRating) {
        return weaponRatingRepository.save(weaponRating);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<WeaponRating> findAll() {
        return weaponRatingRepository.findAllOrdered();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<WeaponRating> findOne(Long id) {
        return weaponRatingRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        weaponRatingRepository.deleteById(id);
    }

    @Override
    public AVGRateProjection getAVGList(Long weaponId) {
        return this.weaponRatingRepository.findAVGRate(weaponId);
    }
}
