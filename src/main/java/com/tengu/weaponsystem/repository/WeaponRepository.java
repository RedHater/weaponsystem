package com.tengu.weaponsystem.repository;

import com.tengu.weaponsystem.entity.Weapon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("http://localhost:4200")
@Repository
public interface WeaponRepository extends CrudRepository<Weapon,Long> {

}
