package com.tengu.weaponsystem.repository;

import com.tengu.weaponsystem.entity.WeaponRating;
import com.tengu.weaponsystem.projection.AVGRateProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("http://localhost:4200")
@Repository
public interface WeaponRatingRepository extends CrudRepository<WeaponRating,Long> {

    @Query("SELECT w from WeaponRating w ORDER BY w.rating DESC ")
    Iterable<WeaponRating> findAllOrdered();

    @Query("Select " +
            "avg(w.rating) as rating, " +
            "avg(w.requirementsForStrengthAndEndurance) as requirementsForStrengthAndEndurance, " +
            "avg(w.requirementForExp) as requirementForExp, " +
            "avg(w.range) as range, " +
            "avg(w.durability) as durability, " +
            "avg(w.repairAbility) as repairAbility, " +
            "avg(w.pace) as pace, " +
            "avg(w.singleHitSpeed) as singleHitSpeed, " +
            "avg(w.defense) as defense, " +
            "avg(w.accuracy) as accuracy, " +
            "avg(w.fintVariety) as fintVariety, " +
            "avg(w.safety) as safety" +
            " from WeaponRating w where w.weapon.id = :weaponId")
    AVGRateProjection findAVGRate(@Param("weaponId") Long weaponId);
}
