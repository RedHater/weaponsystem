package com.tengu.weaponsystem.projection;

public interface AVGRateProjection {
    Double getRating();
    Double getRequirementsForStrengthAndEndurance();
    Double getRequirementForExp();
    Double getRange();
    Double getDurability();
    Double getRepairAbility();
    Double getPace();
    Double getSingleHitSpeed();
    Double getDefense();
    Double getAccuracy();
    Double getFintVariety();
    Double getSafety();
}
