package com.tengu.weaponsystem.entity;


import javax.persistence.*;

@Entity
@Table(name="weaponrating")
public class WeaponRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "weapon")
    private Weapon weapon;

    @Column(name = "Rating")
    private Double rating;

    @Column(name = "REQUIREMENTS_FOR_STRENGTH_AND_ENDURANCE")
    private Double requirementsForStrengthAndEndurance;

    @Column(name = "REQUIREMENT_FOR_EXP")
    private Double requirementForExp;

    @Column(name = "RANGE_")
    private Double range;

    @Column(name = "DURABILITY")
    private Double durability;

    @Column(name = "REPAIR_ABILITY")
    private Double repairAbility;

    @Column(name = "PACE")
    private Double pace;

    @Column(name = "SINGLE_HIT_SPEED")
    private Double singleHitSpeed;


    @Column(name = "DEFENSE")
    private Double defense;

    @Column(name = "ACCURACY")
    private Double accuracy;

    @Column(name = "FINT_VARIETY")
    private Double fintVariety;

    @Column(name = "SAFETY")
    private Double safety;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Double getRequirementsForStrengthAndEndurance() {
        return requirementsForStrengthAndEndurance;
    }

    public void setRequirementsForStrengthAndEndurance(Double requirementsForStrengthAndEndurance) {
        this.requirementsForStrengthAndEndurance = requirementsForStrengthAndEndurance;
    }

    public Double getRequirementForExp() {
        return requirementForExp;
    }

    public void setRequirementForExp(Double requirementForExp) {
        this.requirementForExp = requirementForExp;
    }

    public Double getRange() {
        return range;
    }

    public void setRange(Double range) {
        this.range = range;
    }

    public Double getDurability() {
        return durability;
    }

    public void setDurability(Double durability) {
        this.durability = durability;
    }

    public Double getRepairAbility() {
        return repairAbility;
    }

    public void setRepairAbility(Double repairAbility) {
        this.repairAbility = repairAbility;
    }

    public Double getPace() {
        return pace;
    }

    public void setPace(Double pace) {
        this.pace = pace;
    }

    public Double getSingleHitSpeed() {
        return singleHitSpeed;
    }

    public void setSingleHitSpeed(Double singleHitSpeed) {
        this.singleHitSpeed = singleHitSpeed;
    }

    public Double getDefense() {
        return defense;
    }

    public void setDefense(Double defense) {
        this.defense = defense;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public Double getFintVariety() {
        return fintVariety;
    }

    public void setFintVariety(Double fintVariety) {
        this.fintVariety = fintVariety;
    }

    public Double getSafety() {
        return safety;
    }

    public void setSafety(Double safety) {
        this.safety = safety;
    }
}
