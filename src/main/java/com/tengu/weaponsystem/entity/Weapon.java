package com.tengu.weaponsystem.entity;



import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="weapon")
public class Weapon {

    public enum Hands{
        ДВЕ,ВАРИАТИВНО,ОДНА;
        public static Hands getById(String id){
            for(Hands e : values()) {
                if(e.name().equalsIgnoreCase(id)) return e;
            }
            return ОДНА;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="name")
    private String name;

    @ManyToOne
    @JoinColumn(name="avatarid")
    private Image avatar;

    @Column(name = "Hands_count")
    private Hands handsCount;

    @Column(name="description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getAvatar() {
        return avatar;
    }

    public void setAvatar(Image avatar) {
        this.avatar = avatar;
    }

    public Hands getHandsCount() {
        return handsCount;
    }

    public void setHandsCount(Hands handsCount) {
        this.handsCount = handsCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
