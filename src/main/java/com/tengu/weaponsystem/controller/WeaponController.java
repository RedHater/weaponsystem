package com.tengu.weaponsystem.controller;


import com.tengu.weaponsystem.entity.Weapon;

import com.tengu.weaponsystem.repository.WeaponRepository;
import com.tengu.weaponsystem.service.WeaponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "weapons")
public class WeaponController {

    @Autowired
    private WeaponService weaponService;


    @PostMapping("/save")
    public Weapon saveWeapon(@RequestBody Weapon weapon){
        return this.weaponService.save(weapon);
    }
}
