package com.tengu.weaponsystem.controller;

import com.tengu.weaponsystem.entity.WeaponRating;
import com.tengu.weaponsystem.projection.AVGRateProjection;
import com.tengu.weaponsystem.service.WeaponRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "weaponRatings")
public class WeaponRatingContoller {

    @Autowired
    private WeaponRatingService weaponRatingService;

    @PostMapping("/save")
    public WeaponRating saveWP(@RequestBody WeaponRating weaponRating){
        return this.weaponRatingService.save(weaponRating);
    }
    @GetMapping("/all")
    public Iterable<WeaponRating> findAllinArray(){
        return this.weaponRatingService.findAll();
    }

    @GetMapping("/avgrate/{weaponId}")
    public AVGRateProjection getAVGRate(@PathVariable("weaponId") Long weaponId){ return this.weaponRatingService.getAVGList(weaponId);}
}
